var Game;

var CountryGame;
CountryGame = (function() {

  function CountryGame() {

  }

  CountryGame.prototype.start = function() {
      $('#start, #game-fail').addClass('hidden');
      $('#game').removeClass('hidden');
      Game.timer.runner('reset');
      Game.timer.runner('start');
  }

  CountryGame.prototype.check = function() {
    q_hash = JSON.stringify($('.questions').find('input[name="question[]"]').map(function(index, item) { return $(item).val(); }).toArray());
    a_hash = JSON.stringify($('.answers').find('input[name="answer[]"]').map(function(index, item) { return $(item).val(); }).toArray());
    return a_hash === q_hash;
  };
  

  CountryGame.prototype.won = function() {
    $('#game').addClass('hidden');
    $('#game-fail').addClass('hidden');

    $('#level-time').text(this.timer.runner('info').formattedTime);
    $('#game-success').removeClass('hidden');

  	$.post(App.BaseUrl + 'countries/won', {
  		time: this.timer.runner('info').time,
  		formattedTime: this.timer.runner('info').formattedTime
  	});
  };

  CountryGame.prototype.lost = function() {
    $('#game').addClass('hidden')
    $('#game-fail').removeClass('hidden');
    $('#game-success').addClass('hidden');

  	$.post(App.BaseUrl + 'countries/lost', {
  		time: this.timer.runner('info').time,
  		formattedTime: this.timer.runner('info').formattedTime
  	});
  };

  return CountryGame;

})();

$(document).ready(function() {

	Game = new CountryGame();

	Game.timer = $('#runner').runner();

  $('.answers').sortable({
    axis: "x",
  }).disableSelection();

  $('.js-start-game').on('click', function() {
    Game.start();
  });

  $('.js-check-game').on('click', function() {
  	Game.timer.runner('stop');
  	if (Game.check()) {
  		Game.won();
  	} else {
  		Game.lost();
  	}	
  });

});

