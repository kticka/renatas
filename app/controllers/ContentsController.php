<?php

class ContentsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index($content = 'index')
	{
		if (View::exists('contents.'.$content))
		{
			$data['view'] = $content;
			return View::make('content', $data);
		}
	}

	public function puzzle($puzzle) {
		$data['puzzle']['name'] = $puzzle;
		$data['puzzle']['image'] = Session::get('puzzles.'.$puzzle);
		return View::make('puzzle', $data);
	}

}
