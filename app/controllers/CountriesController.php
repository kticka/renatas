<?php

class CountriesController extends BaseController
{

  public function index()
  {

    if (Input::get("flush")) {
      Session::flush();
    }


    $max_level = $this->getMaxLevel();
    $cur_level = Input::get('level', 1);

    // echo $cur_level."<br>";
    // echo $max_level."<br>";

    if ($cur_level > $max_level) {
      return 'Pries tai laimek mazesnius lygius';
    }


    Session::set('cur_level', $cur_level);

    $level = Config::get('game.levels.' . ($cur_level - 1));

    if (!$level) {
			return \Redirect::route('puzzle', ['puzzle' => \Config::get('game.levels.0.puzzles.0.name')]);
    }

    $data = ['questions' => [], 'answers' => []];

    $data['next_level'] = $this->getCurLevel() + 1;

    $questions = &$data['questions'];
    $answers   = &$data['answers'];

    foreach ($level['countries'] as $country) {
      $hash = md5(str_random(10));
      array_push($questions, ['hash' => $hash, 'image' => $country['image']]);
      array_push($answers, ['hash' => $hash, 'image' => $country['flag'], 'country' => $country['country']]);
    }

    shuffle($answers);

    return View::make('countries.index', $data);
  }

  public function won()
  {
    $level = Config::get('game.levels.' . ($this->getCurLevel() - 1));

    foreach ($level['puzzles'] as $puzzle) {
      Session::set('puzzles.' . $puzzle['name'], $puzzle['image']);
    }

    $this->checkAchievments();

    Session::set('max_level', (Session::get('cur_level', 0) + 1));
    $data           = Input::all();
    $data['action'] = 'won';
    Session::set('history.levels.' . $this->getCurLevel() . '.won', $data);

    return $this->checkAchievments();
  }

  public function lost()
  {
    $data = Input::all();
    Session::push('history.levels.' . $this->getCurLevel() . '.retries', $data);
  }


  private function getCurLevel()
  {
    return Session::get("cur_level", 1);
  }

  private function getMaxLevel()
  {
    return Session::get("max_level", 1);
  }

  private function checkAchievments()
  {
    $newAchievments = [];

    foreach (Config::get('game.achievements.time') as $achievement) {
      if (($this->getMaxLevel() == ($achievement['level'] + 1)) && ($this->getTotalTime() <= $achievement['time'])) {
        if ($this->addAchievement($achievement['achievement'])) {
          array_push($newAchievments, $achievement['achievement']);
        }
      }
    }

    return $newAchievments;
  }

  private function getTotalTime()
  {
    $total_time = 0;
    $history    = Session::get('history.levels', []);
    foreach ($history as $level) {
      if ($data = @$level['won']) {
        $total_time += $data['time'];
      }
    }

    return $total_time;
  }

  private function addAchievement($achievement)
  {
    $key = md5($achievement);

    $exists = Session::get('achievements.' . $key, null);
    if (!$exists) {
      Session::set('achievements.' . $key, $achievement);

      return true;
    }

    return false;
  }

}
