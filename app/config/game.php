<?php
	return [
    'levels' => [
      // Level 1
      [
        'countries' => [
          [
            'country' => 'United Kingdom',
            'image'   => 'bigben.jpg',
            'flag'    => 'uk.jpg'
          ],
          [
            'country' => 'Italy',
            'image'   => 'coloseum.jpg',
            'flag'    => 'italy.jpg'
          ],
          [
            'country' => 'Egypt',
            'image'   => 'pyramids.jpg',
            'flag'    => 'egypt.jpg'
          ],
          [
            'country' => 'France',
            'image'   => 'eiffel.jpg',
            'flag'    => 'france.jpg'
          ],		  
        ],

        'puzzles' => [
          ['name' => 'London', 'image' => 'london.jpg'] ///
        ],
      ],
	  
      // Level 2
      [
        'countries' => [
          [
            'country' => 'United Kingdom',
            'image'   => 'stonehenge.jpg',
            'flag'    => 'uk.jpg'
          ],
          [
            'country' => 'Italy',
            'image'   => 'pisa.jpg',
            'flag'    => 'italy.jpg'
          ],
          [
            'country' => 'USA',
            'image'   => 'empire.jpg',
            'flag'    => 'usa.jpg'
          ],
          [
            'country' => 'China',
            'image'   => 'greatwall.jpg',
            'flag'    => 'china.jpg'
          ],
        ],

        'puzzles' => [
          ['name' => 'London', 'image' => 'london.jpg'] ///
        ],
      ],	

	  
      // Level 3
      [
        'countries' => [
          [
            'country' => 'Greece',
            'image'   => 'acropolis.jpg',
            'flag'    => 'greece.jpg'
          ],
          [
            'country' => 'Egypy',
            'image'   => 'sphinx.jpg',
            'flag'    => 'egypt.jpg'
          ],
          [
            'country' => 'Russia',
            'image'   => 'cathedral.jpg',
            'flag'    => 'russia.jpg'
          ],
          [
            'country' => 'Australia',
            'image'   => 'opera.jpg',
            'flag'    => 'australia.jpg'
          ],
        ],

        'puzzles' => [
          ['name' => 'London', 'image' => 'london.jpg'] ///
        ],
      ],	  

      // Level 4
      [
        'countries' => [
          [
            'country' => 'Brasil',
            'image'   => 'redeemer.jpg',
            'flag'    => 'brasil.jpg'
          ],
          [
            'country' => 'Chile',
            'image'   => 'moai.jpg',
            'flag'    => 'chile.jpg'
          ],
          [
            'country' => 'Emirates',
            'image'   => 'burj.jpg',
            'flag'    => 'emirates.jpg'
          ],
          [
            'country' => 'India',
            'image'   => 'tajmahal.jpg',
            'flag'    => 'india.jpg'
          ],
        ],
        'puzzles' => [
          ['name' => 'Paris', 'image' => 'paris.jpg'] ///
        ],
      ],
    ],

    'achievements' => [
      'time' => [
        ['level' => 1, 'time' => 5000, 'achievement' => 'First level in 5 seconds'],
        ['level' => 2, 'time' => 10000, 'achievement' => '2 levels in 10 seconds'],
		['level' => 3, 'time' => 15000, 'achievement' => '3 levels in 15 seconds'],
		['level' => 4, 'time' => 20000, 'achievement' => '4 levels in 20 seconds'],
      ]
    ]
  ];

  