<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::group(['prefix' => 'countries'], function() {
  Route::get('/', ['uses' => 'CountriesController@index', 'as' => 'countries.index']);
  Route::post('/won', ['uses' => 'CountriesController@won', 'as' => 'countries.won']);
  Route::post('/lost', ['uses' => 'CountriesController@lost', 'as' => 'countries.lost']);
});

Route::group(['prefix' => 'utils'], function() {
  Route::get('flush', ['uses' => 'UtilsController@flush', 'as' => 'utils.flush']);
});

Route::get('/puzzles/{puzzle}', ['uses' => 'ContentsController@puzzle', 'as' => 'puzzle']);
Route::get('/{content?}', ['uses' => 'ContentsController@index', 'as' => 'contents']);


