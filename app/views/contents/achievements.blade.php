@extends('layout')

@section('content')
      


    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Achievements</h3> 

       <div class="box-tools pull-right">
          <a href="{{ route('utils.flush') }}" class="btn btn-xs btn-success">Reset</a>
        </div>
      </div>

      <div class="box-body">
        @foreach(Session::get('achievements', []) as $achievement)

          <div class="alert alert-success">
            <strong>{{ $achievement }}</strong>
          </div>

       @endforeach
   
      </div><!-- /.box-body -->
    </div><!-- /.box -->
   

@endsection