<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Game</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="{{ asset('css/bootstrap.min.css" rel="stylesheet') }}">
    <link href="{{ asset('css/bootstrap-theme.min.css" rel="stylesheet') }}">
    <link href="{{ asset('css/AdminLTE.min.css" rel="stylesheet') }}">
    <link href="{{ asset('css/skin-blue.css" rel="stylesheet') }}">
    <link href="{{ asset('css/custom.css" rel="stylesheet') }}">
  </head>
  <body>

  <body class="hold-transition skin-blue sidebar-mini">
     <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b>LTE</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        
        @include('partials.nav')
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      @include('partials.sidebar')

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

          @yield('content')

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <strong>&nbsp;</strong>
      </footer>

    </div><!-- ./wrapper -->

    <script>
      var App = {};
      App.BaseUrl = '{{ Config::get("app.base_url") }}';
    </script>

    <script src="{{ asset('js/jquery-1.12.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/app.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/country_game.js') }}"></script>
    <script src="{{ asset('js/jquery.runner-min.js') }}"></script>

    @yield('scripts')

    
  </body>
</html>