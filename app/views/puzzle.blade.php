@extends('layout')

@section('content')
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Puzzle {{ $puzzle['name'] }}</h3>
      </div>

      <div class="box-body">
        <div  style="position: relative; margin: 0px auto; width: 750px;">
          <canvas id="canvas"></canvas>
        </div>
  
      </div><!-- /.box-body -->
      <div class="box-footer">
        Footer
      </div><!-- /.box-footer-->
    </div><!-- /.box -->

@endsection

@section('scripts')

  <script src="{{ asset('js/puzzle.js') }}"></script>
  <script>
    $(document).ready(function() {
      initialize_puzzle('{{ asset("img/puzzles/".$puzzle['image']) }}');
    });
  </script>

@endsection