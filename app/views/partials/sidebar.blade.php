<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Levels</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          @for($i=1;$i<=Session::get('max_level', 1);$i++)
            <li><a href="{{ route('countries.index', ['level' => $i]) }}"><i class="fa fa-circle-o"></i> Level {{$i}}</a></li>
          @endfor
        </ul>
      </li>
      <li class="treeview">
        <a href="{{ route('contents', 'achievements') }}">
          <i class="fa fa-graduation-cap"></i> <span>Achievements</span>
        </a>
      </li>
      @if (Session::get('puzzles', null))

      <li class="treeview">
        <a href="#">
          <i class="fa fa-puzzle-piece"></i> <span>Puzzles</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        
        <ul class="treeview-menu">
          @foreach(Session::get('puzzles', []) as $name => $puzzle)
            <li><a href="{{ route('puzzle', $name) }}"><i class="fa fa-circle-o"></i> {{ $name }}</a></li>
          @endforeach
        </ul>
      </li>
      @endif

      <li class="treeview">
        <a href="{{ route('contents', 'faq') }}">
          <i class="fa fa-question-circle"></i> <span>FAQ</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>