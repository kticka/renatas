<div id="game" class="hidden">

  <div class="row questions">
    @foreach($questions as $question)
      <div class="col-md-3">
        <div class="question">
          <input type="hidden" name="question[]" value="{{ $question['hash'] }}">
          <div class="image">
            <img src="{{ asset('img/countries/'.$question['image']) }}">
          </div>
        </div>
      </div>
    @endforeach

  </div>

  <div class="row answers">

    @foreach($answers as $answer)

    <div class="col-md-3">
      <div class="answer">
        <input type="hidden" name="answer[]" value="{{ $answer['hash'] }}">
        <div class="image">
          <img src="{{ asset('img/countries/'.$answer['image']) }}">
        </div>
        {{ $answer['country'] }}
      </div>
    </div>

    @endforeach
    
  </div>

  <div class="row">
    <div class="col-md-12 text-center">
      <div id="runner"></div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-center">
      <button class="btn btn-lg btn-success js-check-game">Check</button>
    </div>
  </div>

</div>