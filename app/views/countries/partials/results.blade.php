 
<div class="row hidden" id="game-success">
  
  <div class="col-md-12 text-center">

    <p><h5>Your time: <span id="level-time"></span></h5></p>
    <p><a class="btn btn-success btn-lg" href="?level={{ $next_level }}">Next level</a></p>
    
  </div>

</div>
  
<div class="row hidden" id="game-fail">
  
  <div class="col-md-12 text-center">

    <p><h5>Wrong</h5></p>
    <p><a class="btn btn-danger btn-lg js-start-game">Retry</a></p>
    
  </div>

</div>
