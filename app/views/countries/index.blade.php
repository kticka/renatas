@extends('layout')


@section('content')

    @if (Input::has('debug'))
      <pre>
        {{ print_r(Session::all(), true) }}
      </pre>
    @endif

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Countries</h3>
      </div>

      <div class="box-body">
      
        @include('countries.partials.start')

        @include('countries.partials.countries')

        @include('countries.partials.results')

      </div><!-- /.box-body -->
      <div class="box-footer">
        Footer
      </div><!-- /.box-footer-->
    </div><!-- /.box -->
   



@stop()